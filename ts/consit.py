# Imports and setup

import json
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

# Create a local StreamingContext
sc = SparkContext("local[2]", "DistinctLicences")

# Set the batch interval in seconds
batch_interval = 60

# Create the streaming context object
ssc = StreamingContext(sc, batch_interval)

# Create the kakfa connection object
KafkaStream = KafkaUtils.createDirectStream(ssc, ["licence"], {"metadata.broker.list": "localhost:9092"})

# Counts instances of records with the same SiteID, and removes those not congested, finally only returning SiteIDs
siteCongestionCounts = KafkaStream.map(lambda tuple: (json.loads(tuple[1])["SiteID"], 1)) \
					.reduceByKey(lambda a, b: a + b) \
					.filter(lambda tuple: tuple[1] > 200).map(lambda tuple: tuple[0])


siteCongestionCounts.saveAsTextFiles("consit")

ssc.start()
ssc.awaitTermination()