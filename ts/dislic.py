# Imports and setup

import json
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

# Create a local StreamingContext
sc = SparkContext("local[2]", "DistinctLicences")

# Set the batch interval in seconds
batch_interval = 60

# Create the streaming context object
ssc = StreamingContext(sc, batch_interval)

# Create the Kakfa connection object
KafkaStream = KafkaUtils.createDirectStream(ssc, ["licence"], {"metadata.broker.list": "localhost:9092"})

# Maps the JSON string to licences, countByValue totals distinct elements
licenceCounts = KafkaStream.map(lambda tuple: json.loads(tuple[1])["Licence"]) \
					.countByValue()

licenceCounts.saveAsTextFiles("dislic")

ssc.start()
ssc.awaitTermination()